# PROPERTY OF BANKOF.CLOUD 2022
# DO NOT COPY, CLONE or REUSE WITHOUT PERMISSION

########################################################
# REMINDER - CHANGE PUBLIC KEY!
########################################################

variable "public_key" {
  type      = string
  default   = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDNJpsMoRXdcvG9YxBr0ROTqyStdJG1OqOZdiuVftkR5x77nxZQShRgdfhojKBUlCWHUp18jykN+wMW75878/vD+A2MLbmet5s2uBELvKrEBdOqI2tWmrropmMebBIGUvXQKoijD7PVMjtza3KbEQaxVbmSgMmqC1r/ka/YEWexbsE40WQPkd4zO9af0jvPSmTFBzgBxf+NKnW0cjp/47M3STUDRYV6PB4oblakDGtAnOcuLPNd7Xk93DOeZt6xyu8lD9nvzzRxVwf48FsgI9PO32MCjnTRBtmLfdT7U1hluJhyDpP3jllrhGR6rHoDRCjfExJVhsL8SvtPiYKq/j4L6n6gAqy4WIswyh6IQQ5tuEO7YvH1DFoKYC+yTd5jHeh5I3azLePJH23kdO64l76hvEXo62M2jkrDS/Xl3C1PW+wEOoDYVoyig13LurqzF32NsKww6vSjvByFYruXYMjtcPvoqaNrvQ/s7Jdu6dlD5Ycv6aITTpMiXLIvMUoddHk= root@b768ebe78284"
}

locals {
  cloud_init_script = <<-EOT
  #!/bin/bash
  curl -kfsSL http://cfd15.bankn.cloud:8091/machines/join-up.sh | sudo bash --
  EOT
}

########################################################
# BofC REQUIRES: use only approved provider verisions!
########################################################

# Configure the AWS provider
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 4.21.0"
    }
  }
	required_version = ">= 1.0"
}

########################################################
# CONTACT BofC IT Helpdesk FOR CREDENTIALS 
########################################################

provider "aws" {
  region = "us-west-2"
  access_key = "{{ .Param "aws/access-key-id" }}"
  secret_key = "{{ .Param "aws/secret-key" }}"
}

resource "aws_key_pair" "rsa-aws" {
  key_name   = "rsa-aws"
  public_key = var.public_key
}

########################################################
# WRITTEN BY BofC SECURITY TEAM - DO NOT MODIFY
########################################################

resource "aws_default_vpc" "default" {
	tags = {
		Name = "Default VPC"
  }
}

resource "aws_security_group" "bofc_basic" {

	name_prefix 	= "bofc"
	description		= "BofC Security"

	vpc_id = aws_default_vpc.default.id
  

  ingress {
		description = "network/firewall-ports[0]"
		from_port   = 22
		to_port     = 22
		protocol    = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
	
  ingress {
		description = "network/firewall-ports[1]"
		from_port   = 443
		to_port     = 443
		protocol    = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
	
	egress {
		from_port   = 0
		to_port     = 0
		protocol    = "-1"
		cidr_blocks = ["0.0.0.0/0"]
	}
	tags = {
		Name = "BofC"
	}
}

########################################################
# BofC AI & HPC Team - 2019
########################################################

resource "aws_instance" "machine" {
  for_each    = toset( ["bofc01", "bofc02", "boc05"] )

  ami           	= "ami-0efdb7f56eef4c641"
  instance_type 	= "t2.micro"
  tags      		= {
    Name            = each.value
    BofCteam        = "ainhpc"
  }
  key_name 		= "rsa-aws"
  
  security_groups = [aws_security_group.bofc_basic.name]
  user_data = local.cloud_init_script

}




